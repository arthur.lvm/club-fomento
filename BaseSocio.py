from pymongo import MongoClient  # el cliente de Mongodb
from socio import Socio     # importo la clase socio
from bson.objectid import ObjectId # convertir un id String en objetoId bson
from bson.json_util import dumps
import time as t

def obtener_bd():
    url = 'mongodb://localhost'
    client = MongoClient (url)
    db = client ['fomento']
    return db

class BaseSocio:

    def insertar(socio):
        bd = obtener_bd()
        collection_socio = bd['socio']
        return collection_socio.insert_one({
            "nombre": socio.nombre,
            "numero": socio.numero,
            "alDia":  socio.alDia
        }).inserted_id

    def actualizar(id, socio):
        bd = obtener_bd()
        resultado = bd.socio.update_one(
            {
            '_id': ObjectId(id)
            }, 
            {
                '$set': {
                    "nombre": socio.nombre,
                    "numero": socio.numero
                }
            })
        return resultado.modified_count

    def obtener():
        bd = obtener_bd()
        return bd.socio.find()

    def obtenerPorNumero (numero):
        bd = obtener_bd()
        r = bd.socio.find_one({'numero' : numero})
        return r

    def obtenerPorNombre (nombre):
        bd = obtener_bd()
        return bd.socio.find({'nombre': nombre})

    def eliminar(id):
        bd = obtener_bd()
        resultado = bd.socio.delete_one(
            {
            '_id': ObjectId(id)
            })
        return resultado.deleted_count
    
    def estaAlDiaNumero(numero):
        bd = obtener_bd()
        resul = bd.socio.find_one({'numero': numero})
        if resul is None:
            return resul
        else:
            return (resul['alDia'])

class Caja:
    
    def sumarCantidad (personas,cantidad):
        bd = obtener_bd()
        resul = bd.cajaDiaria.find_one()
        id = resul['_id']
        bd.cajaDiaria.update_one({"_id": id},{"$inc":{"personas":personas,"total": cantidad}} )
    
    def ponerEnCero ():
        bd = obtener_bd()
        resul = bd.cajaDiaria.find_one()
        id = resul['_id']
        bd.cajaDiaria.update_one({"_id": id}, {"$set": {"personas": 0, "total": 0}})
    
    def cerrarCaja(self):
        bd = obtener_bd()
        resul = bd.cajaDiaria.find_one()
        act = {
            "fecha" : t.strftime("%c"),
            "personas": resul['personas'],
            "total": resul['total']
        }
        bd.cajaTotal.insert_one(act)
        self.ponerEnCero()
    
    def verTotal():
        
        
c = Caja
c.sumarCantidad(50,200)

c.cerrarCaja(c)
"""
obte = b.obtenerPorNombre("jorgelin")
for a in obte:
    print ("El nombre es: {} y el numero es: {}".format(a['nombre'], a['numero']))
j = b.obtener()
for i, key in enumerate(j):
    print (key)
"""








"""list_cur = list(nom)
for each in list_cur:
    print (each)
"""

