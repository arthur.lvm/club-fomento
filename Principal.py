from pymongo import MongoClient
from socio import Socio     
from bson.objectid import ObjectId



def obtener_bd():
    url = 'mongodb://localhost'
    client = MongoClient (url)
    db = client ['fomento']
    return db

def insertar(socio):
    bd = obtener_bd()
    collection_socio = bd['socio']
    return collection_socio.insert_one({
        "nombre": socio.nombre,
        "numero": socio.numero
        }).inserted_id

def actualizar(id, socio):
    bd = obtener_bd()
    resultado = bd.socio.update_one(
        {
        '_id': ObjectId(id)
        }, 
        {
            '$set': {
                "nombre": socio.nombre,
                "numero": socio.numero
            }
        })
    return resultado.modified_count

def obtener():
    bd = obtener_bd()
    return bd.socio.find()

def obtenerPorNumero (numero):
    bd = obtener_bd()
    r = bd.socio.find({numero : numero})
    return r

def obtenerPorNombre (nombre):
    bd = obtener_bd()
    return bd.socio.find({"nombre": nombre})

def eliminar(id):
    bd = obtener_bd()
    resultado = bd.socio.delete_one(
        {
        '_id': ObjectId(id)
        })
    return resultado.deleted_count

r = eliminar("5ffdb47ab848bfbe73e84427")
print (r)